package financeTask.Task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import financeTask.Task.controllers.ApiController;
import financeTask.Task.entities.Client;
import financeTask.Task.services.UserSessionHolder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;

@RunWith(SpringJUnit4ClassRunner.class)
public class ApiControllerTest {
	private MockMvc mockMvc;

	@InjectMocks
	ApiController apiController;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(apiController).build();
	}

	@Test
	public void testApiControllerReceiveLoan() throws Exception {
		HashMap<String, String> args = new HashMap<>();
		args.put("loanSum", "1500");
		args.put("rate", "10");
		args.put("daysOfLoan", "30");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(args);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/financeTaskApi/receiveLoan")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testApiControllerExtendedLoan() throws Exception {
		HashMap<String, String> args = new HashMap<>();
		args.put("id", "1");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(args);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/financeTaskApi/extendLoan")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
}
