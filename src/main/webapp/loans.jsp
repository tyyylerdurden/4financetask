<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="/resources/js/jquery.js"></script>
  <script src="/resources/js/jquery.cookie.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<script>
  function takeLoan() {
    $.ajax({
      url: "/api/financeTaskApi/receiveLoan",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type: "POST",
      data: JSON.stringify({
        loanSum: 150000,
        rate: 10,
        daysOfLoan: 30
      }),
      success: function (data) {
        alert(JSON.stringify(data));
      },
      error: function (data) {
        alert(JSON.stringify(data));
      }
    });
  }

  function extendLoan() {
    $.ajax({
      url: "/api/financeTaskApi/extendLoan",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type: "POST",
      data: JSON.stringify({
        id: $("#id").val()
      }),
      success: function (data) {
        alert(JSON.stringify(data));
      },
      error: function (data) {
        alert(JSON.stringify(data));
      }
    });
  }
</script>
<body>
${loans}
<br><br>
<button onclick="takeLoan()">Take loan of 1500$ for 30 days 10% interest rate</button>
<br><br>
<button onclick="extendLoan()">Extend loan for 30 days</button> ID: <input id="id" size="2">
</body>
</html>
