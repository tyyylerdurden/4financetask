package financeTask.Task.entities;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Table(name = "ip")
public class Ip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Identity strategy is needed for Hibernate to increase values in Database like: n, n + 1, n + 2, ..., n + z
    private long ipId;
    @Column(columnDefinition = "VARCHAR(30)")
    private String ip;
    private int attemptsPerDay;

    public long getIpId() { return ipId; }
    public void setIpId(long ipId) { this.ipId = ipId; }

    public String getIp() { return ip; }
    public void setIp(String ip) { this.ip = ip; }

    public int getAttemptsPerDay() { return attemptsPerDay; }
    public void setAttemptsPerDay(int attemptsPerDay) { this.attemptsPerDay = attemptsPerDay; }

    @Override
    public String toString() {
        return "";
    }
}
