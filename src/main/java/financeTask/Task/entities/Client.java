package financeTask.Task.entities;

import javax.persistence.*;

@Entity
@Table(name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Identity strategy is needed for Hibernate to increase values in Database like: n, n + 1, n + 2, ..., n + z
    private long clientId;
    @Column(columnDefinition = "VARCHAR(30)")
    private String name;
    @Column(columnDefinition = "VARCHAR(50)")
    private String password;

    public long getClientId() { return clientId; }
    public void setClientId(long clientId) { this.clientId = clientId; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    @Override
    public String toString() {
        return "";
    }
}
