package financeTask.Task.entities;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Table(name = "loans")
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Identity strategy is needed for Hibernate to increase values in Database like: n, n + 1, n + 2, ..., n + z
    private long loanId;
    private int amount;
    private int rate;
    private int mustPayBack;
    private int paidBack;
    private int daysOfLoan;
    private int extendedDaysAmount;
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateOfLoan;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateLoanEnds;

    private boolean paid;

    @ManyToOne
    @JoinColumn(name = "clientId", nullable = false, updatable = false, referencedColumnName = "clientId")
    private Client client;

    public long getLoanId() { return loanId; }
    public void setLoanId(long loanId) { this.loanId = loanId; }

    public int getAmount() { return amount; }
    public void setAmount(int amount) { this.amount = amount; }

    public int getRate() { return rate; }
    public void setRate(int rate) { this.rate = rate; }

    public int getMustPayBack() { return mustPayBack; }
    public void setMustPayBack(int mustPayBack) { this.mustPayBack = mustPayBack; }

    public int getPaidBack() { return paidBack; }
    public void setPaidBack(int paidBack) { this.paidBack = paidBack; }

    public int getDaysOfLoan() { return daysOfLoan; }
    public void setDaysOfLoan(int daysOfLoan) { this.daysOfLoan = daysOfLoan; }

    public int getExtendedDaysAmount() { return extendedDaysAmount; }
    public void setExtendedDaysAmount(int extendedDaysAmount) { this.extendedDaysAmount = extendedDaysAmount; }

    public DateTime getDateOfLoan() { return dateOfLoan; }
    public void setDateOfLoan(DateTime dateOfLoan) { this.dateOfLoan = dateOfLoan; }

    public boolean isPaid() { return paid; }
    public void setPaid(boolean paid) { this. paid = paid; }

    public DateTime getDateLoanEnds() { return dateLoanEnds; }
    public void setDateLoanEnds(DateTime dateLoanEnds) { this.dateLoanEnds = dateLoanEnds; }

    public Client getClient() { return client; }
    public void setClient(Client client) { this.client = client; }

    @Override
    public String toString() {
        return "ID: " + this.getLoanId() + ";"
                + "Rate: " + (double) this.getRate() + "%;"
                + "Days: " + this.getDaysOfLoan() + ";"
                + "Extended days: " + this.getExtendedDaysAmount() + ";"
                + "Paid Back: " + this.getPaidBack() + ";"
                + "Must Pay Back: " + this.getMustPayBack();
    }
}
