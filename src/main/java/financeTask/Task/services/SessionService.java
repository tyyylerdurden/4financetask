package financeTask.Task.services;

import financeTask.Task.entities.Ip;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class SessionService {
    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addAttempts(HttpServletRequest request) {
        String ip = request.getRemoteAddr().equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : request.getRemoteAddr();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(Ip.class);
        Root<Ip> c = q.from(Ip.class);
        TypedQuery<Ip> queryIp = em.createQuery(q.select(c).where(cb.equal(c.get("ip"), ip)));
        List<Ip> ipResults = queryIp.getResultList();

        Ip ipEntity = ipResults.get(0);
        ipEntity.setAttemptsPerDay(ipEntity.getAttemptsPerDay() + 1);

        HttpSession session = request.getSession();
        int attemptsBefore = (int) session.getAttribute("loanAttemptsFromIp");
        request.getSession().setAttribute("loanAttemptsFromIp", attemptsBefore + 1);

        em.merge(ipEntity);
    }
}
