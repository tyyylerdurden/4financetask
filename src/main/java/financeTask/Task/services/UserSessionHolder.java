package financeTask.Task.services;

import financeTask.Task.entities.Client;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSessionHolder {
    protected Client client;

    public Client getClient() { return client; }
    public void setClient(Client client) { this.client = client; }
}
