package financeTask.Task.services;

import financeTask.Task.entities.Ip;
import financeTask.Task.entities.Loan;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class SchedulerService {
    @Autowired
    Environment env;
    @PersistenceContext
    EntityManager em;

    @Scheduled(fixedDelay = 1000 * 60 * 30)
    @Transactional
    public void checkLoans() {
        if (!Boolean.valueOf(env.getProperty("webserviceScheduler.enabled"))) {
            return;
        }

        DateTime now = new DateTime();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(Loan.class);
        Root<Loan> c = q.from(Loan.class);
        TypedQuery<Loan> query = em.createQuery(q.select(c).where(cb.isFalse(c.get("paid"))));
        List<Loan> results = query.getResultList();

        results.stream().forEach(v -> {
            if (now.isAfter(v.getDateLoanEnds()) && !v.isPaid()) {
               //We must send an email to those who haven't paid
            }
        });
    }

    //Scheduled every day at 00:01 in order to drop down all attempts from ip
    @Scheduled(cron = "0 01 00 ? * *")
    @Transactional
    public void dropAttempts() {
        if (!Boolean.valueOf(env.getProperty("webserviceScheduler.enabled"))) {
            return;
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(Ip.class);
        Root<Ip> c = q.from(Ip.class);
        TypedQuery<Ip> query = em.createQuery(q.select(c));
        List<Ip> results = query.getResultList();

        results.stream().forEach(v -> {
            v.setAttemptsPerDay(0);
            em.merge(v);
        });
    }
}
