package financeTask.Task.services;

import financeTask.Task.entities.Client;
import financeTask.Task.entities.Ip;
import financeTask.Task.entities.Loan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class UserDaoService {
    @PersistenceContext
    private EntityManager em;

    public void insert(Client client){
        em.persist(client);
        em.flush();
    }

    public void insert(Loan loan){
        em.persist(loan);
        em.flush();
    }

    public void insert(Ip ip){
        em.persist(ip);
        em.flush();
    }
}
