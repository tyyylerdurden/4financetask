package financeTask.Task.controllers;

import financeTask.Task.entities.Client;
import financeTask.Task.entities.Ip;
import financeTask.Task.services.UserDaoService;
import financeTask.Task.services.UserSessionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AuthlessController {
    @Autowired
    UserSessionHolder userSessionHolder;
    @Autowired
    UserDaoService userDaoService;
    @PersistenceContext
    EntityManager em;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public Object register(@RequestParam(required = false) String login, String password, HttpServletRequest request) {
        if ((login == null || login.equals(""))
                || (password == null || password.equals(""))) {
            return "provide login and password!";
        } else {
            Client client = new Client();
            client.setName(login);
            client.setPassword(password);

            try {
                List<GrantedAuthority> additionalAuthorities = new ArrayList<>();
                additionalAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));

                User principal = new User(login, password, additionalAuthorities);
                SecurityContext context = SecurityContextHolder.getContext();
                context.setAuthentication(new UsernamePasswordAuthenticationToken(login, Math.random() * 500000, principal.getAuthorities()));

                userDaoService.insert(client);
                userSessionHolder.setClient(client);

                // saving loanAttempts from an ip to the session
                String ip = request.getRemoteAddr().equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : request.getRemoteAddr();
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery q = cb.createQuery(Ip.class);
                Root<Ip> c = q.from(Ip.class);
                TypedQuery<Ip> queryIp = em.createQuery(q.select(c).where(cb.equal(c.get("ip"), ip)));
                List<Ip> ipResults = queryIp.getResultList();
                HttpSession httpSession = request.getSession();

                if (ipResults.size() == 1) {
                    httpSession.setAttribute("loanAttemptsFromIp", ipResults.get(0).getAttemptsPerDay());
                } else {
                    httpSession.setAttribute("loanAttemptsFromIp", 0);
                    Ip ipEntity = new Ip();
                    ipEntity.setIp(ip);
                    ipEntity.setAttemptsPerDay(0);
                    userDaoService.insert(ipEntity);
                }

                return new ModelAndView("redirect:/loans");
            } catch (NullPointerException ex) {
                ex.printStackTrace();
                return new ModelAndView("redirect:/");
            }
        }
    }

    @RequestMapping(value = "/enter", method = RequestMethod.GET)
    public String login(@RequestParam(required = true) String login, String password, HttpServletRequest request) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery q = cb.createQuery(Client.class);
            Root<Client> c = q.from(Client.class);
            TypedQuery<Client> query = em.createQuery(q.select(c).where(cb.equal(c.get("name"), login)));
            List<Client> results = query.getResultList();

            if (results.size() == 1) {
                List<GrantedAuthority> additionalAuthorities = new ArrayList<>();
                additionalAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));

                User principal = new User(login, password, additionalAuthorities);
                SecurityContext context = SecurityContextHolder.getContext();
                context.setAuthentication(new UsernamePasswordAuthenticationToken(login, Math.random() * 500000, principal.getAuthorities()));

                userSessionHolder.setClient(results.get(0));

                // saving loanAttempts from an ip to the session
                String ip = request.getRemoteAddr().equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : request.getRemoteAddr();
                CriteriaBuilder cb2 = em.getCriteriaBuilder();
                CriteriaQuery q2 = cb2.createQuery(Ip.class);
                Root<Ip> c2 = q2.from(Ip.class);
                TypedQuery<Ip> queryIp = em.createQuery(q2.select(c2).where(cb2.equal(c2.get("ip"), ip)));
                List<Ip> ipResults = queryIp.getResultList();
                HttpSession httpSession = request.getSession();

                if (ipResults.size() == 1) {
                    httpSession.setAttribute("loanAttemptsFromIp", ipResults.get(0).getAttemptsPerDay());
                } else {
                    httpSession.setAttribute("loanAttemptsFromIp", 0);
                    Ip ipEntity = new Ip();
                    ipEntity.setIp(ip);
                    ipEntity.setAttemptsPerDay(0);
                    userDaoService.insert(ipEntity);
                }

                return "redirect:/loans";
            } else {
                return "redirect:/";
            }
        } catch (Exception e) {

        }

        return "";
    }

    @RequestMapping(value = "/logoff")
    public String logout() {
        userSessionHolder.setClient(null);
        SecurityContextHolder.getContext().setAuthentication(null);

        return "redirect:/";
    }
}
