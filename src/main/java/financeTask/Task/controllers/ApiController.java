package financeTask.Task.controllers;

import financeTask.Task.entities.Client;
import financeTask.Task.entities.Loan;
import financeTask.Task.services.SessionService;
import financeTask.Task.services.UserDaoService;
import financeTask.Task.services.UserSessionHolder;
import financeTask.Task.webauth.RiskAnalysis;
import org.joda.time.DateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.Map;

@RequestMapping("/api/financeTaskApi")
@ResponseBody
@Controller
public class ApiController {
    @Autowired
    UserSessionHolder userSessionHolder;
    @Autowired
    UserDaoService userDaoService;
    @Autowired
    SessionService sessionService;
    @PersistenceContext
    EntityManager em;

    public static final double extraInterestPerWeek = 0.015; // 1,5%
    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @RiskAnalysis //Annotation for the Handler to Intercept the api call and make a risk analysis
    @RequestMapping(value = "/receiveLoan", method = RequestMethod.POST)
    public String receiveLoan(@RequestBody Map<String, String> args, HttpServletRequest request) {
        DecimalFormat df = new DecimalFormat("#.##");

        double loanSum = Double.parseDouble(args.get("loanSum")) / 100;
        double rate = Double.parseDouble(args.get("rate")) / 100;
        double mustPayBack = Double.parseDouble(df.format(loanSum + (loanSum * rate)).replace(",", "."));
        int daysOfLoan = Integer.parseInt(args.get("daysOfLoan"));
        DateTime loanStarts = new DateTime();
        DateTime loanEnds = loanStarts.plusDays(daysOfLoan);

        Client client;

        try {
            client = userSessionHolder.getClient();
        } catch (Exception e) {
            client = new Client(); //for tests
        }

        Loan loan = new Loan();
        loan.setClient(client);
        loan.setAmount((int) loanSum * 100);
        loan.setDaysOfLoan(daysOfLoan);
        loan.setRate(Integer.parseInt(args.get("rate")));
        loan.setPaid(false);
        loan.setMustPayBack((int) mustPayBack * 100);
        loan.setPaidBack(0);
        loan.setDateOfLoan(loanStarts);
        loan.setDateLoanEnds(loanEnds);
        loan.setExtendedDaysAmount(0);

        try {
            userDaoService.insert(loan);
            sessionService.addAttempts(request);

            return "Your loan of sum has been transfered to your bank account. " +
                    "Your loan can be extended for 30 days more for the interest rate of 1.5% per week to the main sum";
        } catch (Exception e) {
            logger.error("ERROR " + e);

            return "Something went wrong!";
        }
    }

//    @RiskAnalysis SHOULD IT BE?
    @Transactional
    @RequestMapping(value = "/extendLoan", method = RequestMethod.POST)
    public String extendLoan(@RequestBody Map<String, String> args) {
        DecimalFormat df = new DecimalFormat("#.##");

        long loanId = Long.parseLong(args.get("id"));
        int daysForExtend = 30; // the loan is extended for 30 days

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(Loan.class);
        Root<Loan> c = q.from(Loan.class);
        TypedQuery<Loan> query = em.createQuery(q.select(c).where(cb.equal(c.get("loanId"), loanId)));

        try {
            Loan loan = query.getSingleResult();

            double remainsWithoutPercent = ((double) loan.getMustPayBack() / 100) / ((double) (100 + loan.getRate()) / 100); // Counting the sum of remain a client must pay back without the old rate
            double oldInterestRate = (double) loan.getRate() / 100;
            double newInterestRate = oldInterestRate + ((extraInterestPerWeek / 7) * daysForExtend); // To count how many percent will be added to previous rate considering 1.5% per week and total amount of days
            double remainsWithNewPercent = remainsWithoutPercent + (remainsWithoutPercent * newInterestRate);

            newInterestRate = Double.parseDouble(df.format(newInterestRate).replace(",", "."));
            remainsWithNewPercent = Double.parseDouble(df.format(remainsWithNewPercent).replace(",", "."));

            loan.setExtendedDaysAmount(loan.getExtendedDaysAmount() + daysForExtend);
            loan.setDateLoanEnds(loan.getDateOfLoan().plusDays(loan.getDaysOfLoan()).plusDays(daysForExtend));
            loan.setRate((int) (newInterestRate * 100));
            loan.setMustPayBack((int) remainsWithNewPercent * 100);
            em.merge(loan);

            return "Your credit has been extended and recounted according to the rate of 1.5% per week for 30 days!";
        } catch (Exception e) {
            logger.error("ERROR " + e);
            return "Probably the loan does not exist!";
        }
    }
}
