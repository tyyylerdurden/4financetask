package financeTask.Task.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("Page controller")
public class PageController {
    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Register or login, please";
    }
}
