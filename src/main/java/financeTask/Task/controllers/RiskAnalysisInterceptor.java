package financeTask.Task.controllers;

import financeTask.Task.exceptions.InappropriateLoanTime;
import financeTask.Task.exceptions.MoreThanThreeAttempts;
import financeTask.Task.webauth.RiskAnalysis;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RiskAnalysisInterceptor extends HandlerInterceptorAdapter {
    @PersistenceContext
    EntityManager em;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws InappropriateLoanTime, MoreThanThreeAttempts, ParseException {
        if (handler instanceof HandlerMethod) {
            Method method = ((HandlerMethod) handler).getMethod();
            RiskAnalysis riskAnalysis = method.getAnnotation(RiskAnalysis.class);
            if (riskAnalysis != null) {
                //checking attempts from ip
                int attempts = (int) request.getSession().getAttribute("loanAttemptsFromIp");
                if (attempts == 3) {
                    throw new MoreThanThreeAttempts("YOU REACHED THE MAXIMUM(3) ATTEMPTS OF MAKING A LOAN TODAY!");
                }

                //checking attempts of loan from 0:00 till 9:00
                Date date = new Date() ;
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm") ;
                dateFormat.format(date);

                if (dateFormat.parse(dateFormat.format(date)).after(dateFormat.parse("00:00"))
                        && dateFormat.parse(dateFormat.format(date)).before(dateFormat.parse("09:00"))) {
                    throw new InappropriateLoanTime("ATTEMPT OF MAKING A LOAN AFTER 00:00");
                }
            }
        }

        return true;
    }
}
