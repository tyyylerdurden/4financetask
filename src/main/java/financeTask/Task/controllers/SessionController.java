package financeTask.Task.controllers;

import financeTask.Task.entities.Client;
import financeTask.Task.entities.Loan;
import financeTask.Task.services.UserSessionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.util.List;

@Controller
public class SessionController {
    @Autowired
    UserSessionHolder userSessionHolder;
    @PersistenceContext
    EntityManager em;

    @RequestMapping("/loans")
    public Object loans(Model model) throws IOException {
        Client client = userSessionHolder.getClient();

        if (client == null) {
            return new ModelAndView("redirect:/");
        } else {

            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery q = cb.createQuery(Loan.class);
            Root<Loan> c = q.from(Loan.class);
            TypedQuery<Loan> query = em.createQuery(q.select(c).where(cb.equal(c.get("client"), client)));
            List<Loan> resultLoan = query.getResultList();

            model.addAttribute("loans", resultLoan);

            return "loans";
        }
    }
}
