package financeTask.Task.webauth;

import financeTask.Task.services.UserSessionHolder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public UserSessionHolder userSessionHolder() {
        return new UserSessionHolder();
    }
}
