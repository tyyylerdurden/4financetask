package financeTask.Task.exceptions;

public class InappropriateLoanTime extends Exception {
    public InappropriateLoanTime(String s) {
        super(s);
    }
}
