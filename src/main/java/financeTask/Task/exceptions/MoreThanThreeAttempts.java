package financeTask.Task.exceptions;

public class MoreThanThreeAttempts extends Exception {
    public MoreThanThreeAttempts(String s) {
        super(s);
    }
}
